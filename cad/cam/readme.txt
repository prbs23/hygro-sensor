hygro-sensor-B.Cu.gbl      - Bottom Copper Layer
hygro-sensor-B.Mask.gbs    - Bottom Solder Mask
hygro-sensor-PTH.drl       - NC Drill - Plated
hygro-sensor-Edge.Cuts.gm1 - Board Outline
hygro-sensor-F.Cu.gtl      - Top Copper Layer
hygro-sensor-F.Mask.gts    - Top Solder Mask
hygro-sensor-F.SilkS.gto   - Top Silk Screen

Notes:
No Bottom silkscreen layer.
