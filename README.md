<div align="center">![Completed hygro-sensor](doc/hygro_sensor.jpg)</div>

# hygo-sensor

This project is a build a small, inexpensive hygrometer where the actual humidity sensor was separated from the display unit. Where possible power consumption has been optimized to maximize the battery life of the single AA battery used for power. Currently the design gets around 3 months of continuous runtime before draining a standard AA battery.
