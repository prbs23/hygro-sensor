#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <display.h>
#include <i2c.h>

#define HYGRO_ADDR (0b1000000)
uint8_t write_data[8];
uint8_t read_data[8];

// When the humidity read finishes, update the display
void update_display(uint8_t count, uint8_t *data) {
    uint32_t humidity;
    humidity = (((uint32_t)data[1]) << 8) | data[0];
    humidity = (humidity * 10000) / 0xffff;
    if (humidity <= 9999)
        set_display_uint(humidity, 2, false);
    else
        set_display_uint(1000, 1, false);
}

// Every 2 seconds start reading the humidity
ISR(TCA0_OVF_vect) {
    i2c_write_read_data(HYGRO_ADDR, 1, write_data, 2, read_data, &update_display);
    TCA0.SINGLE.INTFLAGS = 1;
};

int main(void) {
    // Set main clock divider to 64.
    CCP = CCP_IOREG_gc;
    CLKCTRL.MCLKCTRLB = (0x5<<1) | 1;

    // Set up Peripherals
    setup_display(250);
    i2c_setup(0);

    // Configure TCA0 to generate a 2 second interrupt
    TCA0.SINGLE.PER = 39374;
    TCA0.SINGLE.INTCTRL = 1;
    TCA0.SINGLE.CTRLA = (4<<1) | 1;

    // Enable idle sleep mode
    SLPCTRL.CTRLA = 1;

    // Enable interrupts
    sei();
    
    // Configure the HDC2080 Hygrometer
    write_data[0] = 0x0E;
    write_data[1] = 0b101 << 4;
    write_data[2] = (0b10<<1) | 1;
    i2c_write_data(HYGRO_ADDR, 3, write_data);
    while (i2c_active()) {};
    write_data[0] = 0x02;
    i2c_write_read_data(HYGRO_ADDR, 1, write_data, 2, read_data, &update_display);

    // Sleep forever. Interrupts will wake us up
    while (1) {
        asm("sleep");
    };

    return 0;
};
