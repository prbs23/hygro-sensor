#include "i2c.h"
#include <avr/io.h>
#include <avr/interrupt.h>

uint8_t _i2c_addr;
uint8_t _i2c_operation;
uint8_t _i2c_write_count;
uint8_t *_i2c_write_data;
uint8_t _i2c_read_count;
uint8_t *_i2c_read_data;
uint8_t _i2c_trans_count;
void (*_i2c_pkt_cb)(uint8_t count, uint8_t *data);

ISR(TWI0_TWIM_vect) {
    if (_i2c_operation & 1) {
        if (_i2c_trans_count < _i2c_write_count) {
            TWI0.MDATA = _i2c_write_data[_i2c_trans_count++];
        } else if (_i2c_operation == 3) {
            _i2c_operation = 2;
            _i2c_trans_count = 0;
            TWI0.MADDR = _i2c_addr | 1;
        } else {
            TWI0.MCTRLB = 3;
            _i2c_operation = 0;
        };
    } else if (_i2c_operation == 2) {
        _i2c_read_data[_i2c_trans_count] = TWI0.MDATA;
        _i2c_trans_count++;
        if (_i2c_trans_count < _i2c_read_count) {
            TWI0.MCTRLB = 2;
        } else {
            TWI0.MCTRLB = 4 | 3;
            _i2c_operation = 0;
            if (_i2c_pkt_cb != 0)
                _i2c_pkt_cb(_i2c_read_count, _i2c_read_data);
        };
    };
};

void i2c_setup(uint8_t baud) {
    TWI0.MBAUD = baud;
    TWI0.MCTRLA = (1<<7) | (1<<6) | (1<<0);
    TWI0.MSTATUS = 1;
};

void i2c_write_data(uint8_t tgt_addr, uint8_t count, uint8_t data[]) {
    _i2c_addr = tgt_addr<<1;
    _i2c_operation = 1;
    _i2c_write_count = count;
    _i2c_write_data = data;
    _i2c_trans_count = 0;
    TWI0.MADDR = _i2c_addr;
};

void i2c_read_data(uint8_t tgt_addr, uint8_t count, uint8_t data[], void (*pkt_done_cb)(uint8_t count, uint8_t *data)) {
    _i2c_addr = tgt_addr<<1;
    _i2c_operation = 2;
    _i2c_read_count = count;
    _i2c_read_data = data;
    _i2c_pkt_cb = pkt_done_cb;
    _i2c_trans_count = 0;
    TWI0.MADDR = _i2c_addr | 1;
};

void i2c_write_read_data(uint8_t tgt_addr, uint8_t wr_count, uint8_t wr_data[], uint8_t rd_count, uint8_t rd_data[], void (*pkt_done_cb)(uint8_t count, uint8_t *data)) {
    _i2c_addr = tgt_addr<<1;
    _i2c_operation = 3;
    _i2c_write_count = wr_count;
    _i2c_write_data = wr_data;
    _i2c_read_count = rd_count;
    _i2c_read_data = rd_data;
    _i2c_pkt_cb = pkt_done_cb;
    _i2c_trans_count = 0;
    TWI0.MADDR = _i2c_addr;
};

bool i2c_active() {
    return _i2c_operation != 0;
};

