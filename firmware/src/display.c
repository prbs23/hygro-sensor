#include "display.h"
#include <avr/io.h>
#include <avr/interrupt.h>

uint8_t display_segments[4];

uint8_t number_lut[11][4] = {{0b01, 0b11, 0b10, 0b11},
                             {0b00, 0b10, 0b10, 0b00},
                             {0b01, 0b01, 0b11, 0b10},
                             {0b01, 0b10, 0b11, 0b10},
                             {0b00, 0b10, 0b11, 0b01},
                             {0b01, 0b10, 0b01, 0b11},
                             {0b01, 0b11, 0b01, 0b11},
                             {0b00, 0b10, 0b10, 0b10},
                             {0b01, 0b11, 0b11, 0b11},
                             {0b01, 0b10, 0b11, 0b11},
                             {0b01, 0b01, 0b01, 0b11}};

uint8_t display_phase = 0;

ISR(TCB0_INT_vect) {
    int seg = display_phase>>1;
    if (display_phase & 1) {
        PORTC.OUT = 1<<seg;
        PORTA.OUT = (~display_segments[seg])<<1;
        PORTB.OUT = (PORTB.OUT&0xdf) | (((~display_segments[seg])>>2) & 0x20);
    } else {
        PORTC.OUT = 0;
        PORTA.OUT = (display_segments[seg])<<1;
        PORTB.OUT = (PORTB.OUT&0xdf) | (((display_segments[seg])>>2) & 0x20);
    };
    display_phase = (display_phase + 1) & 0x7;
    TCB0.INTFLAGS = 1;
};

void setup_display(uint16_t update_rate_hz) {
    clear_display();
    PORTC.DIR = 0xf;
    PORTA.DIR |= 0xfe;
    PORTB.DIR |= 0x20;
    TCB0.CCMP = F_CPU / update_rate_hz;
    TCB0.INTCTRL = 1;
    TCB0.CTRLA = (1<<6) | 1;
};

void set_display_uint(uint16_t value, uint8_t decimal_position, bool colon) {
    clear_display();

    if (value > 9999) {
        // Show EEEE if the value is too wide for the display
        for (int i = 0; i < 4; i++) for (int j = 0; j < 4; j++) display_segments[j] |= (number_lut[10][j]<<(i*2));
    } else {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) display_segments[j] |= number_lut[value % 10][j] << ((3-i)*2);
            value = value/10;
            if (value == 0)
                break;
        };

        if (decimal_position != 0)
            display_segments[0] |= 1 << (((3 - decimal_position) * 2) + 1);
        if (colon)
            display_segments[0] |= 0x80;
    };
};

void set_display_int(int16_t value, uint8_t decimal_position, bool colon) {
    clear_display();

    if ((value > 999) || (value < -999)) {
        // Show EEEE if the value is too wide for the display
        for (int i = 0; i < 4; i++) for (int j = 0; j < 4; j++) display_segments[j] |= (number_lut[10][j]<<(i*2));
    } else {
        if (value < 0) {
            display_segments[2] |= 1;
            value = value * -1;
        };
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 4; j++) display_segments[j] |= number_lut[value % 10][j] << ((3-i)*2);
            value = value/10;
            if (value == 0)
                break;
        };
        
        if (decimal_position != 0)
            display_segments[0] |= 1 << (((3 - decimal_position) * 2) + 1);
        if (colon)
            display_segments[0] |= 0x80;
    };
};

void clear_display() {
    for (int i = 0; i < 4; i++) display_segments[i] = 0;
};

