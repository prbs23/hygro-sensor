#ifndef I2C_H
#define I2C_H

#include <stdint.h>
#include <stdbool.h>

void i2c_setup(uint8_t baud);
void i2c_write_data(uint8_t addr, uint8_t count, uint8_t data[]);
void i2c_read_data(uint8_t addr, uint8_t count, uint8_t data[], void (*pkt_done_cb)(uint8_t count, uint8_t *data));
void i2c_write_read_data(uint8_t addr, uint8_t write_count, uint8_t write_data[], uint8_t read_count, uint8_t read_data[], void (*pkt_done_cb)(uint8_t count, uint8_t *data));
bool i2c_active();

#endif //I2C_H
