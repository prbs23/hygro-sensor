#ifndef DISPLAY_H
#define DISPLAY_H

#include <stdint.h>
#include <stdbool.h>

void setup_display(uint16_t update_rate_hz);
void set_display_uint(uint16_t value, uint8_t decimal_position, bool colon);
void set_display_int(int16_t value, uint8_t decimal_position, bool colon);
void clear_display();

#endif //DISPLAY_H
